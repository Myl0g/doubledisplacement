# What Is This?

A website to help Chemistry students get comfortable with stoichiometry and double displacement reactions.

## What Kinds of Problems Do I Get?

* Solving the actual displacement reaction
* Calculating the limiting reactant
* Getting the amounts of the products

## Developer's Manual

### Building

The various project components are stored under `src/`, but the actual code executed is `public/main.js`.

`build.sh` installs all `yarn` dependencies, uses TSLint to fix the styling of the various files under `src/`, and then uses `npx webpack` to compile and bundle them under `public/main.js`.

If you find yourself forgetting to run the script: create the file `.git/hooks/pre-commit` and enter in:

```bash
#! /bin/sh

bash ./build.sh
```

The script will now run even before you commit.

### Adding to the Equations Bank

The Equations Bank is stored in the variable `equationsBank` under `src/index.ts`, and is comprised of `DoubleDisplacement` objects.

Each `DoubleDisplacement` object is comprised of 4 `Compound` objects: 2 reactants and 2 products.

Each `Compound` object must be passed the following constructor arguments: `coefficient, str, molar, amount, units`.

* coefficient: Self-descriptive.
* str: the string/textual representation of the Compound. Does not include the coefficient.
* molar: the molar mass of the `Compound`. See [this calculator](http://www.bmrb.wisc.edu/metabolomics/mol_mass.php).
* amount: the amount in `units` (the next argument) of the `Compound`.
  * if this is not specified, this Compound will be treated as a product and the amount calculated automatically from the reactants.
* units: either `"moles"` or `"grams"`.
