#! /usr/bin/env bash

set -e

yarn install --production=false # installs all developer dependencies

npx tslint -c tslint.json --fix 'src/**/*.ts' # lints all src files
npx webpack-cli # compiles typescript & produces minified .js in dist/

echo -e "// This code is compiled. Check out git.io/doubledisplacement for the real deal.\n$(cat public/main.js)" > public/main.js
