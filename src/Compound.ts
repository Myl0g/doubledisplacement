import { sigDigs } from "./helpers";

/*
    To understand Compound, I'll use examples.

    If you have the compound 2KI,
    * coefficient = 2
    * str = "KI"
    * molar (mass) = 166
    * amount = however much of the Compound you have (assumed to be a product if not specified)
    * units = "moles" or "grams" (assumed to be a product if not specified)

    The moles and grams properties will calculate the amount of the product in moles or grams
    regardless of the actual units specified. Molar mass must be specified for this to work.
*/
class Compound {
  constructor(
    public coefficient: number,
    public str: string,
    public molar: number,
    public amount: number,
    public units: string,
  ) {}

  /*
     The moles and grams properties use the conversion factor
     "grams * (1 mol / X grams)" or "moles * (X grams / 1 mol)"
  */
  get moles(): number {
    return this.getUnits("moles");
  }
  get grams(): number {
    return this.getUnits("grams");
  }

  // Represents the Compound in ASCII and not Unicode, so a standard keyboard can type it.
  get typeable(): string {
    if (this.coefficient === 1) {
      // No need to type 1 in front of expression
      return this.str;
    }

    return `${this.coefficient}${this.str}`;
  }

  private getUnits(desired: string): number {
    if (this.units === desired) {
      return this.amount;
    }

    if (this.units === "moles") {
      return sigDigs(this.amount * this.molar, 3);
    } else {
      return sigDigs(this.amount / this.molar, 3);
    }
  }
}

/*
    toString is called whenever the object is attempted to be used as a string.
    In this case, the Compound is represented in Unicode for display purposes.
*/
Compound.prototype.toString = function() {
  // For each number x, subscript form of x = subscripts[x - 1] where x > 0.
  const subscripts = [
    "\u2081",
    "\u2082",
    "\u2083",
    "\u2084",
    "\u2085",
    "\u2086",
    "\u2087",
    "\u2088",
    "\u2089",
  ];

  // Clones str
  let str: string = this.str.slice(0);

  for (let i = 0; i < str.length; i++) {
    const testInt = parseInt(str.charAt(i), 10);

    // True if testInt isn't NaN
    if (testInt) {
      // Take the original string up to i, put in the subscript, then continue past i
      str = `${str.slice(0, i)}${subscripts[testInt - 1]}${str.slice(i + 1)}`;
    }
  }

  if (this.coefficient === 1) {
    // No need to type 1 in front of expression
    return str;
  }

  return `${this.coefficient}${str}`;
};

export default Compound;
