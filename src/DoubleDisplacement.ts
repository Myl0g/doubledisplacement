import Compound from "./Compound";
import { numLength, sigDigs } from "./helpers";

/*
    A DoubleDisplacement object has 4 Compounds: 2 reactants and 2 products (in that order).

    The limiting reactant is calculated immediately upon construction.

    The reactants and products properties return a string of the two, separated by " + ".
    The toString function separates these two properties with a " -> ".

    productAmounts(units) finds products without amounts specified by multiplying the limiting reactant's amount
    by the relevant mole ratio.
*/

class DoubleDisplacement {
  public limiter: Compound;
  constructor(
    public rx1: Compound,
    public rx2: Compound,
    public p1: Compound,
    public p2: Compound,
  ) {
    this.limiting();
  }

  // Typeable forms are used for reactants and products for easy response comparison.
  public reactants(typeable): string {
    return this.getSide([this.rx1, this.rx2], typeable);
  }

  public products(typeable): string {
    return this.getSide([this.p1, this.p2], typeable);
  }

  public productAmounts(requestedUnits): void {
    const productList = [this.p1, this.p2];

    for (const product of productList) {
      if (!product.amount) {
        product.units = requestedUnits;

        product.amount =
          this.limiter.moles * (product.coefficient / this.limiter.coefficient);

        if (requestedUnits === "grams") {
          product.amount *= product.molar;
        }
      }
    }
  }

  private limiting(): void {
    const test1: number = this.rx1.moles / this.rx1.coefficient;
    const test2: number = this.rx2.moles / this.rx2.coefficient;

    if (test1 > test2) {
      this.limiter = this.rx2;
    } else {
      this.limiter = this.rx1;
    }
  }

  // Not a client function
  private getSide(sideList, typeable): string {
    if (typeable) {
      return `${sideList[0].typeable} + ${sideList[1].typeable}`;
    }

    return `${sideList[0]} + ${sideList[1]}`;
  }
}

DoubleDisplacement.prototype.toString = function() {
  // \u2192 is a rightward arrow
  return `${this.reactants()} \u2192 ${this.products()}`;
};

export default DoubleDisplacement;
