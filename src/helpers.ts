export let sigDigs = (num, digits): number =>
  parseFloat(Number.parseFloat(num).toPrecision(digits));

// Gets random item from a list "li".
export let randListItem = (li): any =>
  li[Math.floor(Math.random() * li.length)];

export let numLength = (num): number => num.toString().length;

export function request(question: string, correctDisplay: string, correct: string): void {
  let response = prompt(question);
  if (!response) {
    response = "nothing";
  }

  if (response === correct) {
    alert("Correct!");
  } else {
    alert(`Nope. You put ${response} when it was actually ${correctDisplay}.`);
  }
}

export function requestImperfect(question: string, correctDisplay: string, correct: string): void {
  let response = prompt(question);
  if (!response) {
    response = "nothing";
  }

  const difference = Math.abs(parseFloat(response) - parseFloat(correct));

  if (difference <= 2) {
    alert("Correct!");
  } else {
    alert(`Nope. You put ${response} when it was actually ${correctDisplay}.`);
  }
}
