import Compound from "./Compound";
import DoubleDisplacement from "./DoubleDisplacement";
import { randListItem, request, requestImperfect } from "./helpers";

const equationsBank = [
  new DoubleDisplacement(
    new Compound(1, "Pb(NO3)2", 331.21, 2.5, "moles"),
    new Compound(2, "KI", 166.0, 1.25, "moles"),
    new Compound(1, "PbI2", 461.03, NaN, ""),
    new Compound(2, "K(NO3)", 101.103, NaN, ""),
  ),
  new DoubleDisplacement(
    new Compound(1, "Pb(NO3)2", 331.21, 1.065, "grams"),
    new Compound(1, "Na2CO3", 105.98, 0.545, "grams"),
    new Compound(1, "Pb(CO3)", 267.22, NaN, ""),
    new Compound(2, "Na(NO3)", 84.99, NaN, ""),
  ),
  new DoubleDisplacement(
    new Compound(1, "Pb(NO3)2", 331.21, 1.004, "grams"),
    new Compound(1, "NaOH", 39.99, 1.0093, "grams"),
    new Compound(1, "Pb(OH)2", 241.231, NaN, ""),
    new Compound(2, "Na(NO3)", 84.99, NaN, ""),
  ),
];

// Basic chemical reaction question (double displacement).
function requestProducts(eq: DoubleDisplacement): void {
  const question = `What is yielded by ${eq.rx1} and ${
    eq.rx2
  } reacting? Balance your answers, and make sure to keep the order\
  (i.e. if Pb is the first reactant, then it's also the first product).`;
  const correct = `${eq.products(true)}`;
  const correctDisplay = `${eq.products(false)}`;

  request(question, correctDisplay, correct);
}

// Basic limiting reactant question.
function requestLR(eq: DoubleDisplacement): void {
  const question = `Use the equation ${eq}. If you have ${eq.rx1.amount} ${
    eq.rx1.units
  } of ${eq.rx1} and ${eq.rx2.amount} ${eq.rx2.units} of ${
    eq.rx2
  }, what is the limiting reactant?`;
  const correct = `${eq.limiter.typeable}`;
  const correctDisplay = `${eq.limiter}`;

  request(question, correctDisplay, correct);
}

// Asks for the product amounts.
function requestProductAmounts(eq: DoubleDisplacement): void {
  const wanted = randListItem([eq.p1, eq.p2]);
  const wantedUnit = randListItem(["grams", "moles"]);
  eq.productAmounts(wantedUnit);

  const question = `Given the equation ${eq} and ${eq.limiter.amount} ${
    eq.limiter.units
  } of ${eq.limiter}, how many ${wantedUnit} of ${wanted} are formed?`;

  let correct = "";
  if (wantedUnit === "grams") {
    correct = `${wanted.grams}`;
  } else {
    correct = `${wanted.moles}`;
  }

  requestImperfect(question, correct, correct);
}

function genProblem(): void {
  const eq: DoubleDisplacement = randListItem(equationsBank);
  requestProducts(eq);
  requestLR(eq);
  requestProductAmounts(eq);
}

// Once the page loads, the program waits until the HTML-generated button is clicked before executing.
window.onload = () => document.getElementById("wrapper").addEventListener("click", genProblem);
